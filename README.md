# ClearSHARE UI App

This application provides the UI integration for the `clearshared` API
server in the ClearOS Server marketplace. The basic functionality is:

1. Start/Stop `clearshared` and the decentralized storage network daemons.
2. Configure firewall rules for `clearshared` and the decentralized storage networks.
3. View status information for the API server `clearshared` and storage network daemons.

## Installing/Updating

Because `clearshared` is a self-contained python package, it doesn't need its
own separate package for installation/upgrades. Instead, this `app-` package
handles that. Installing `clearshared` involves the following:

1. Copy `clearshare.sh` to `$PATH`
2. Run `clearshare.sh install` to create a python virtual environment and install
   the `clearshared` package.
3. Copy `clearshared.service` to `/lib/systemd/system` to configure `systemctl`
   to manage the daemon lifecycle.

## Syncing Problems

If the sia node does not sync the blockchain at first, it may have an issue with
the gateway connecting to peers. If this happens, try to run the following
commands:
```
siac gateway connect 84.232.168.234:9981
siac gateway connect 72.69.188.134:9981
siac gateway connect 88.212.168.101:9981
siac gateway connect 23.239.8.40:8081
```
This establishes a set of outbound peers to connect to and bootstrap the node's
local network.