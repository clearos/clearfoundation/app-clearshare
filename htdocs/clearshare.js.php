<?php

/**
 * clearshare javascript helper.
 * @category   Apps
 * @package    clearshare
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('base');
clearos_load_language('digitalworld');

///////////////////////////////////////////////////////////////////////////////
// J A V A S C R I P T
///////////////////////////////////////////////////////////////////////////////

header('Content-Type:application/x-javascript');
?>

var service_status = false;


$(document).ready(function() {
  
    clearosGetClearshareSiaStatus();
});

// Functions
//----------

function clearosGetClearshareSiaStatus() {
    $.ajax({
        url: '/app/clearshare/sia/get_sia_progress_status',
        method: 'GET',
        dataType: 'json',
        success : function(payload) {
                handleDigitalworldRes(payload);
                window.setTimeout(clearosGetClearshareSiaStatus, 5000);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            window.setTimeout(clearosGetClearshareSiaStatus, 5000);
        }
    });
}

function handleDigitalworldRes(payload) {

    if (payload.results.Synced == 'Synced: No') {
        $("#server_stop_warning").hide();
        $("#sia_progress-container").show();
        $("#progress_field").show();
        $("#progress_title").show();
        $('#progress_text').text(payload.results.progress_status);

        var percentage_value = payload.results.percentage_value;
        var height = payload.results.height;
        var currentblock = payload.results.blocksizelimit;

        $('#synced_text').text("No");
        $('#height_text').text(height); 
        $('#blocksizelimit_text').text(currentblock);  

        if (percentage_value < 100) {
            $("#sia_progress")
              .css("width", percentage_value + "%")
              .attr("aria-valuenow", percentage_value)
              .text(percentage_value + "% Synced");

        } else {
            $('#synced_text').text("Yes");
            $("#sia_progress-container").hide();
            $("#progress_field").hide();
            $("#progress_title").hide();

        }

    } else if (payload.results.Synced == 'Synced: Yes') {
        $('#synced_text').text("Yes"); 
        $("#sia_progress-container").hide();
        $("#progress_field").hide();
        $("#progress_title").hide();

    } else {
        $("#server_stop_warning").show();
        $("#sia_progress-container").hide();
        $("#progress_field").hide();
        $("#progress_title").hide();
    }
}