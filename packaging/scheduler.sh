#!/bin/bash -l
# Just setup some colors for the terminal to make it less error prone.
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
blue=`tput setaf 4`
yellow=`tput setaf 3`
cyan=`tput setaf 6`

export HOST_RENTABLE_STORAGE=/var/lib/siad-data/
ETC_CLEARSHARE="/etc/clearos/clearshare.d"

export SCHEDULER_LOG_FILE=/var/log/scheduler.log

function echoLog() {
    echo $@ | tee -a $SCHEDULER_LOG_FILE
}


if [ ! -d $ETC_CLEARSHARE ]; then
    echoLog "${blue}TASK${reset}    Creating $ETC_CLEARSHARE"
    mkdir -p $ETC_CLEARSHARE
fi

# This script works as a scheduler for the sequence of tasks required
# to the get ClearSHARE node up and running.
#
# 1. Check siad has loaded
# 2. Boostrap the Sia blockchain and sync the remaining blocks.
# 3. Restore a wallet from derived keey seed.
# 4. Wait for a rescan of the wallet transactions.
# 5. Setup default storage and collateral and announce the host.

# Between each of these events, we need to wait.

# Checks if siad has completed loading
function isSiadLoaded() {
    echoLog -n "${blue}CHECK${reset}   Has siad finished fully loading? "
    RESULT=`curl -s -A "Sia-Agent" "localhost:9980/wallet" | jq .message -r`
    echoLog $RESULT
    if [ "$RESULT" == "490 Module not loaded - Refer to API.md" ]; then
        return 1; 
    else 
        return 0; 
    fi
}

# Checks with the local consensus endpoint to see if the blockchain
# has finished syncing.
function isBlockchainSynced() {
    echoLog -n "${blue}CHECK${reset}   Has blockchain finished syncing? "
    RESULT=`curl -s -A "Sia-Agent" "localhost:9980/consensus" | jq .synced`
    echoLog $RESULT
    if [ "$RESULT" == "true" ]; then
        return 0; 
    else 
        return 1; 
    fi
}

# Restores the wallet from a derived seed in ClearLIFE. Note that this
# function just uses the CLI from ClearSHARE to handle the crypto and
# wallet restoration. The CLI makes the request to the local sia host
# API endpoint.
function restoreWalletFromSeed() {
    clearshare account
}

# Checks with the local Sia endpoint if the wallet has been unlocked.
function isWalletUnlocked() {
    echoLog -n "${blue}CHECK${reset}   Is wallet unlocked? "
    export WALLET_API=`curl -s -A "Sia-Agent" "localhost:9980/wallet"`
    RESULT=`echo $WALLET_API | jq .unlocked`
    echoLog $RESULT
    if [ "$RESULT" == "true" ]; then
        WALLET_HEIGHT=$(echo $WALLET_API | jq -r .height)
        CONSENSUS_HEIGHT=$(curl -s -A "Sia-Agent" "localhost:9980/consensus" | jq -r .height)
        echoLog -n "${blue}CHECK${reset}   Is wallet synced to blockchain? "
        if [ "$WALLET_HEIGHT" == "$CONSENSUS_HEIGHT" ]; then
            echoLog Yes
            return 0;
        else
            echoLog No
            echoLog "${cyan}INFO${reset}    Wallet not in sync. Current height is $WALLET_HEIGHT, target is $CONSENSUS_HEIGHT. Waiting..."
            return 1
        fi
    else 
        RESCAN=`echo $WALLET_API | jq .rescanning`
        if [ "$RESCAN" == "true" ]; then
            echoLog "${cyan}INFO${reset}    Wallet rescan is still happening."
        else
            ENCRYPTED=`echo $WALLET_API | jq .encrypted`
            if [ "$ENCRYPTED" == "true" ]; then
                echoLog "${blue}TASK${reset}    Unlocking existing wallet"
                # We have restored the wallet, but it isn't unlocked yet; just
                # perform a simple unlock operation.
                clearshare unlock
            else
                echoLog "${blue}TASK${reset}    Starting wallet restore from derived seed"
                restoreWalletFromSeed
            fi
        fi

        return 1; 
    fi
}

# Makes an API request via ClearSHARE CLI to fund the newly created
# wallet address with some SC to use as collateral for the first/initial
# host contracts.
function fundWallet() {
    clearshare fund
}

# Retrieves progress on account funding from the x.clearshare.network
# server that requested the funds send.
function getFundProgress() {
    clearshare progress
}

# Checks if the wallet has a non-zero balance. Assuming our users have no
# experience doing this, a zero balance is a reasonable check for initial
# funding having been initiated. Possible return values are:
# 0: Wallet has been funded.
# 1: Wallet has not been funded; it is also not in progress of being funded.
# 2: Wallet funding has been requested; waiting for the block to write and
#    the transaction to be confirmed.
function isWalletFunded() {
    echoLog -n "${blue}CHECK${reset}   Wallet funding progress? "
    PROGRESS=$(getFundProgress | tail -1)
    PROGRESS_CODE=`echo $PROGRESS | jq .code`

    if [ $PROGRESS_CODE -eq "0" ]; then
        fundWallet
        return 1
    elif [ $PROGRESS_CODE -eq "1" ]; then
        export BALANCE=`curl -s -A "Sia-Agent" "localhost:9980/wallet" | jq .confirmedsiacoinbalance`
        if [ $BALANCE == '"0"' ]; then
            echoLog "${yellow}WAITING${reset} For funding transaction to propagate through network."
            return 3
        else
            return 0
        fi
    else
        return 2
    fi
}

# Provisions the first 1TB of data automatically for the user. This can 
# be resized later from the ClearLIFE mobile app. The folder $HOST_RENTABLE_STORAGE
# is statically configured during the installation of the app-clearshare UI.
function provisionStorage() {
    siac host folder add $HOST_RENTABLE_STORAGE 1TB >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE
}

# Checks whether the host has already provisioned any storage. As long
# as the amount of committed storage > 0, this returns 1.
function isStorageProvisioned() {
    STORAGE_STATUS=`curl -s -A "Sia-Agent" "localhost:9980/host/storage" | jq .folders`
    if [ "$STORAGE_STATUS" == "null" ]; then
        provisionStorage
        echoLog "${blue}TASK${reset}    Provisioning storage from scratch."
    else
        echoLog "${green}DONE${reset}    Storage is already provisioned."
    fi
}

# Checks with the siastats website for the current price per TB so we
# set competitive initial collateral settings.
function getCurrentPriceTB() {
    export SIA_TOP100=`curl -s https://siastats.info/dbs/top100.json`
    export SIA_STORAGE_PRICE=`echo $SIA_TOP100 | jq .storageprice`
    export SIA_UPLOAD_PRICE=`echo $SIA_TOP100 | jq .upload`
    export SIA_DOWNLOAD_PRICE=`echo $SIA_TOP100 | jq .download`
    export SIA_COLLATERAL=`echo $SIA_TOP100 | jq .collateral`
}

# Sets the default config parameters using the latest data from the Top 100
# highest rated hosts on the Sia network (according to siastats.info).
# Prices are scaled to 175% of Top 100 prices and collateral is 175% of storage.
function setDefaultConfig() {
    getCurrentPriceTB

    SIA_STORAGE_PRICE=$(echo "scale=2; $SIA_STORAGE_PRICE*1.75" | bc)
    SIA_UPLOAD_PRICE=$(echo "scale=2; $SIA_UPLOAD_PRICE*1.75" | bc)
    SIA_DOWNLOAD_PRICE=$(echo "scale=2; $SIA_DOWNLOAD_PRICE*1.75" | bc)
    SIA_COLLATERAL=$(echo "scale=2; $SIA_STORAGE_PRICE*1.75" | bc)

    echoLog "${blue}TASK${reset}    Configure storage price / TB / month to $SIA_STORAGE_PRICE SC"
    siac host config minstorageprice "${SIA_STORAGE_PRICE}SC" >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE
    echoLog "${blue}TASK${reset}    Configure upload price / TB to $SIA_UPLOAD_PRICE SC"
    siac host config minuploadbandwidthprice "${SIA_UPLOAD_PRICE}SC" >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE
    echoLog "${blue}TASK${reset}    Configure download price / TB to $SIA_DOWNLOAD_PRICE SC"
    siac host config mindownloadbandwidthprice "${SIA_DOWNLOAD_PRICE}SC" >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE
    echoLog "${blue}TASK${reset}    Configure collateral / TB / month to $SIA_COLLATERAL SC"
    siac host config collateral "${SIA_COLLATERAL}SC" >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE

    echoLog "${blue}TASK${reset}    Set that we are accepting contracts"
    siac host config acceptingcontracts true >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE
}

# Extracts the hostname being used for dynamic DNS configuration
# on the ClearOS server.
function getDynamicDnsName() {
    export DDNS=`grep INTERNET_HOSTNAME /etc/clearos/network.conf | cut -d'"' -f 2`
}

# Extracts the current Sia hostname
function getSiaHostName() {
    SIA_HOST_NAME=$(siac host -v | grep 'netaddress' | awk '{print $2}')
    export SIA_HOST_NAME=${SIA_HOST_NAME%:*}
}

# Checks if the host has already announced itself with the current DDNS.
# Since announcing costs a transaction fee, we only want to do this once.
# Note that if the host is has not been announced, or is announced with a
# different IP or DDNSthis function will automatically announce itself.
function isHostAnnounced() {
    getDynamicDnsName
    getSiaHostName
    echoLog "${green}OK${reset}      Found dynamic DNS name as $DDNS"
    echoLog "${green}OK${reset}      Found dynamic SIA Host Name name as $SIA_HOST_NAME"
    if [ "$DDNS" == "$SIA_HOST_NAME" ]; then
        # The host status does *not* recommend reannouncing. Don't do anything.
        echoLog "${green}OK${reset}      Host status is OK. Not announcing."
    else
        echoLog "${blue}TASK${reset}    Announce the host to the Sia network"
        echoLog siac host announce "${DDNS}:9982"
        siac host announce "${DDNS}:9982" >> $SCHEDULER_LOG_FILE 2>>$SCHEDULER_LOG_FILE
    fi
}

# Runs the main while loop that waits for the blockchain to finish
# syncing, and then for the wallet to become unlocked, and then for
# the initial funds for collateral to be added to the wallet.
function runScheduler() {
    TOTAL_WAIT=0
    SYNC_WAIT=0
    LOAD_WAIT=0
    WALLET_WAIT=0
    FUNDING_WAIT=0

    # Check progress on the individual steps; then take appropriate action.
    while ! isSiadLoaded
    do
        sleep 5
        TOTAL_WAIT=$(( TOTAL_WAIT + 5 ))
        LOAD_WAIT=$(( SYNC_WAIT + 5 ))

        echoLog "${yellow}WAITING${reset} For siad to finished loading. Total wait $LOAD_WAIT s"
    done
    echoLog "${green}DONE${reset}    Siad was ready after $LOAD_WAIT s"

    while ! isBlockchainSynced
    do
        sleep 5
        TOTAL_WAIT=$(( TOTAL_WAIT + 5 ))
        SYNC_WAIT=$(( SYNC_WAIT + 5 ))

        echoLog "${yellow}WAITING${reset} For blockchain to finished syncing. Total wait $SYNC_WAIT s"
    done
    echoLog "${green}DONE${reset}    Blockchain sync was ready after $SYNC_WAIT s"

    while ! isWalletUnlocked
    do
        sleep 5
        TOTAL_WAIT=$(( TOTAL_WAIT + 5 ))
        WALLET_WAIT=$(( WALLET_WAIT + 5 ))

        echoLog "${yellow}WAITING${reset} For wallet rescan and unlock. Total wait $WALLET_WAIT s"
    done
    echoLog "${green}DONE${reset}    Wallet rescan and unlock was ready after $WALLET_WAIT s"

    # Temporarily save the seed to a file. To be removed when phone unlock mechanism is reliable
    echo "SIA_WALLET_PASSWORD=$(siac wallet seeds | tail -n 1)" > /etc/clearos/clearshare.d/sia.seed

    while ! isWalletFunded
    do
        # Wallet funding has to wait for the block to write; which takes almost
        # 10 minutes. No need to check with the API every 5 seconds...
        sleep 60
        TOTAL_WAIT=$(( TOTAL_WAIT + 60 ))
        FUNDING_WAIT=$(( FUNDING_WAIT + 60 ))

        echoLog "${yellow}WAITING${reset} For wallet initial funding. Total wait $FUNDING_WAIT s"
    done
    echoLog "${green}DONE${reset}    Wallet initial funding completed after $FUNDING_WAIT s"

    # This provisioning check will also setup the initial 1TB of available
    # data for contracts if it hasn't been done before.
    isStorageProvisioned
    # Get the latest prices and configuration for the Top 100 hosts worldwide.
    setDefaultConfig
    # Announce the host to the network now that we are configured.
    isHostAnnounced

    echoLog "${green}DONE${reset}    Wallet, storage and blockchain ready after $TOTAL_WAIT s"
}

echoLog "$(date) Scheduler started"

# Exit immediately if the scheduler has already been successfully run
SCHEDULER_HAS_COMPLETED_FILE="/var/clearos/clearshare/scheduler.run"
if [ -f "$SCHEDULER_HAS_COMPLETED_FILE" ]; then
    echoLog "Scheduler has already completed successfully. Exiting ....."
    exit
fi

# bail is siad is not fully running
if ! siac host &> /dev/null; then
    exit 0
fi

LOCK_FILE="/tmp/clearshare-setup.lock"

if [ -f $LOCK_FILE ]; then
    PID=`cat $LOCK_FILE`
    ps $PID | grep $(basename $0) &>/dev/null
    if [ $? -eq 0 ]; then
        echoLog "${yellow}LOCKED${reset}  ClearSHARE setup is already running at PID $PID"
        exit
    fi
fi
echoLog $$ > $LOCK_FILE

echoLog "$(date) Scheduler running"

# Activate the virtualenv so that we have the clearshare CLI
source /var/clearos/envs/clearshare/bin/activate

runScheduler

touch "$SCHEDULER_HAS_COMPLETED_FILE"

rm -f $LOCK_FILE

rm -f /etc/cron.d/clearshare-scheduler
