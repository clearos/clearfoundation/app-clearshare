
Name: app-clearshare
Epoch: 1
Version: 0.0.57
Release: 1%{dist}
Summary: ClearSHARE
License: Proprietary
Group: Applications/Apps
Packager: Clearfoundation
Vendor: Clearfoundation
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-clearlife
Requires: app-base >= 1:2.10.1
Requires: app-incoming-firewall >= 1:2.5.1

%description
ClearSHARE - This application provides the UI integration for the clearshared API server in the ClearOS Server marketplace.

%package core
Summary: ClearSHARE - API
License: Proprietary
Group: Applications/API
Requires: app-base-core
Requires: app-clearlife-core
Requires: app-network
Requires: clearshared
Requires: rsync
Requires: jq
Requires: sia >= 1.5.9

%description core
ClearSHARE - This application provides the UI integration for the clearshared API server in the ClearOS Server marketplace.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/clearshare
cp -r * %{buildroot}/usr/clearos/apps/clearshare/
rm -f %{buildroot}/usr/clearos/apps/clearshare/README.md

install -d -m 0755 %{buildroot}/etc/clearos/clearshare.d
install -d -m 0755 %{buildroot}/var/clearos/clearshare
install -D -m 0644 packaging/app-clearshare.cron %{buildroot}/etc/cron.d/app-clearshare
install -D -m 0440 packaging/app-clearshare.sudoers %{buildroot}/etc/sudoers.d/app-clearshare
install -D -m 0644 packaging/clearshare.conf.clearos %{buildroot}/etc/clearos/clearshare.conf
install -D -m 0755 packaging/clearshare.cron %{buildroot}/etc/cron.daily/app-clearshare
install -D -m 0644 packaging/clearshare.logrotate %{buildroot}/etc/logrotate.d/clearshare
install -D -m 0644 packaging/clearshare.rsyslog %{buildroot}/etc/rsyslog.d/clearshare.conf
install -D -m 0644 packaging/clearshared.php %{buildroot}/var/clearos/base/daemon/clearshared.php
install -D -m 0644 packaging/filewatch-clearshare-siad-pid.event %{buildroot}/etc/clearsync.d/filewatch-clearshare-siad-pid.conf
install -D -m 0644 packaging/filewatch-clearshare-prices.event %{buildroot}/etc/clearsync.d/filewatch-clearshare-prices.conf
install -D -m 0755 packaging/network-configuration.event %{buildroot}/var/clearos/events/network_configuration/clearshare
install -D -m 0755 packaging/scheduler.sh %{buildroot}/usr/sbin/clearshare-scheduler.sh
install -D -m 0644 packaging/siad.php %{buildroot}/var/clearos/base/daemon/siad.php

%post
logger -p local6.notice -t installer 'app-clearshare - installing'

%post core
logger -p local6.notice -t installer 'app-clearshare-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/clearshare/deploy/install ] && /usr/clearos/apps/clearshare/deploy/install
fi

[ -x /usr/clearos/apps/clearshare/deploy/upgrade ] && /usr/clearos/apps/clearshare/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearshare - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearshare-api - uninstalling'
    [ -x /usr/clearos/apps/clearshare/deploy/uninstall ] && /usr/clearos/apps/clearshare/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/clearshare/controllers
/usr/clearos/apps/clearshare/htdocs
/usr/clearos/apps/clearshare/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/clearshare/packaging
%exclude /usr/clearos/apps/clearshare/unify.json
%dir /usr/clearos/apps/clearshare
%dir /etc/clearos/clearshare.d
%dir /var/clearos/clearshare
/usr/clearos/apps/clearshare/deploy
/usr/clearos/apps/clearshare/language
/usr/clearos/apps/clearshare/libraries
/etc/cron.d/app-clearshare
/etc/sudoers.d/app-clearshare
%config(noreplace) /etc/clearos/clearshare.conf
/etc/cron.daily/app-clearshare
/etc/logrotate.d/clearshare
/etc/rsyslog.d/clearshare.conf
/var/clearos/base/daemon/clearshared.php
/etc/clearsync.d/filewatch-clearshare-siad-pid.conf
/etc/clearsync.d/filewatch-clearshare-prices.conf
/var/clearos/events/network_configuration/clearshare
/usr/sbin/clearshare-scheduler.sh
/var/clearos/base/daemon/siad.php
