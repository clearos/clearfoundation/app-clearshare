<?php

$lang['clearshare_app_name'] = 'ClearSHARE';
$lang['clearshare_app_description'] = 'ClearSHARE - This application provides the UI integration for the clearshared API server in the ClearOS Server marketplace.';
$lang['clearshare_app_info_box'] = 'Check status in the ClearLIFE mobile app:<br> Settings > ClearNODE Apps > ClearSHARE Status';
$lang['clearshare_app_synced'] = 'Synced';
$lang['clearshare_app_height'] = 'Height';
$lang['clearshare_app_progress'] = 'Progress';
$lang['clearshare_app_blocksizelimit'] = 'Block';
$lang['clearshare_app_target'] = 'Target';
$lang['clearshare_app_difficulty'] = 'Difficulty';
$lang['clearshare_app_difficulty'] = 'Difficulty';
$lang['clearshare_app_aritical_errors'] = 'Critical Errors';
$lang['clearshare_app_sia_status'] = 'Please start Server Service';
