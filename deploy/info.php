<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'clearshare';
$app['version'] = '0.0.57';
$app['release'] = '1';
$app['vendor'] = 'Clearfoundation'; // e.g. Acme Co
$app['packager'] = 'Clearfoundation'; // e.g. Gordie Howe
$app['license'] = 'Proprietary'; // e.g. 'GPLv3';
$app['license_core'] = 'Proprietary'; // e.g. 'LGPLv3';
$app['description'] = lang('clearshare_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('clearshare_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = lang('base_subcategory_decentralized');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-clearlife',
    'app-base >= 1:2.10.1',
    'app-incoming-firewall >= 1:2.5.1',
);

$app['core_requires'] = array(
    'app-clearlife-core',
    'app-network',
    'clearshared',
    'rsync',
    'jq',
    'sia >= 1.5.9',
);

$app['core_file_manifest'] = array(
    'clearshared.php' => array('target' => '/var/clearos/base/daemon/clearshared.php'),
    'siad.php' => array('target' => '/var/clearos/base/daemon/siad.php'),
    'clearshare.cron'=> array(
        'target' => '/etc/cron.daily/app-clearshare',
        'mode' => '0755'
    ),
    'clearshare.rsyslog' => array('target' => '/etc/rsyslog.d/clearshare.conf'),
    'scheduler.sh'=> array(
        'target' => '/usr/sbin/clearshare-scheduler.sh',
        'mode' => '0755'
    ),
    'app-clearshare.sudoers' => array(
        'target' => '/etc/sudoers.d/app-clearshare',
        'mode' => '0440',
    ),
    'filewatch-clearshare-siad-pid.event' => array(
        'target' => '/etc/clearsync.d/filewatch-clearshare-siad-pid.conf',
    ),
    'filewatch-clearshare-prices.event' => array(
        'target' => '/etc/clearsync.d/filewatch-clearshare-prices.conf',
    ),
    'app-clearshare.cron' => array( 'target' => '/etc/cron.d/app-clearshare' ),
    'clearshare.logrotate' => array('target' => '/etc/logrotate.d/clearshare'),
    'clearshare.conf.clearos'=> array(
        'target' => '/etc/clearos/clearshare.conf',
        'config' => TRUE,
        'config_params' => 'noreplace',
    ),
    'network-configuration.event'=> array(
        'target' => '/var/clearos/events/network_configuration/clearshare',
        'mode' => '0755'
    ),
);

$app['core_directory_manifest'] = array(
    '/etc/clearos/clearshare.d' => array(),
    '/var/clearos/clearshare' => array(),
);
