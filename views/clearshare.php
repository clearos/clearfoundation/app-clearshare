<?php

/**
 * ClearSHARE Default View.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('clearshare');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo infobox_highlight(lang('clearshare_app_name'), lang('clearshare_app_info_box'));
