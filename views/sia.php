<?php

/**
 * Sia Default View.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('clearshare');

echo "<div id='server_stop_warning' style='display:none;'>";
echo infobox_warning(lang('base_warning'), lang('clearshare_app_sia_status'));
echo "</div>";

echo "<h3 id='progress_title' style='clear: both;'>Progress</h3>\n";
echo "<div>\n";
echo progress_bar('sia_progress', array('input' => 'progress'));
echo "</div>\n";

///////////////////////////////////////////////////////////////////////////////
// Form open
///////////////////////////////////////////////////////////////////////////////
    echo form_open('');
    echo form_header('Server Status');

///////////////////////////////////////////////////////////////////////////////
// Form fields and buttons
///////////////////////////////////////////////////////////////////////////////

$status = $consensus_status['synced'];

if ($status) {
    $synced_status = 'Yes';
} else {
    $synced_status = 'No';
}

$target = implode(",", $consensus_status['target']); 

echo field_input('synced', $synced_status, lang('clearshare_app_synced'), TRUE);
echo field_input('height', $consensus_status['height'], lang('clearshare_app_height'), TRUE);
echo '<div id="progress_field">';
echo field_input('progress', '', lang('clearshare_app_progress'), TRUE);
echo '</div>';
echo field_input('blocksizelimit', $consensus_status['currentblock'], lang('clearshare_app_blocksizelimit'), TRUE);
//echo field_input('target', $target, lang('clearshare_app_target'), TRUE);
//echo field_input('difficulty', $consensus_status['difficulty'], lang('clearshare_app_difficulty'), TRUE);



echo field_button_set();

///////////////////////////////////////////////////////////////////////////////
// Form close
///////////////////////////////////////////////////////////////////////////////

echo form_footer();
echo form_close();
