<?php

/**
 * ClearSHARE default controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearSHARE controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage Controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 */

class Clearshare extends ClearOS_Controller
{
    /**
     * ClearSHARE default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('clearshare');

        // Load views
        //-----------

        $views = array('clearshare/server', 'clearshare/network', 'clearshare/clearshare_host', 'clearshare/sia');

        $this->page->view_controllers($views, lang('clearshare_app_name'));
    }
}
