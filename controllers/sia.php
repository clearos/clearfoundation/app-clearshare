<?php

/**
 * Sia default controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearSHARE controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage Controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 */

class Sia extends ClearOS_Controller
{
    /**
     * Sia default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Sia');

        try {

            $response = $this->sia->get_siac_consensus();

            if ($response) {
                $data['sia_api_status_stop'] = FALSE;
                $data['consensus_status'] = $response;

            } else {
                $data['sia_api_status_stop'] = TRUE;
                $data['consensus_status'] = NULL;
            }

            $this->page->view_form('sia', $data, lang('clearshare_app_name'));

        } catch (Exception $e) {
            $data['sia_api_status_stop'] = TRUE;
            $this->page->view_form('sia', $data, lang('clearshare_app_name'));
        }
        
    }

    /**
     * Get Progress estimated.
     *
     * @return json
     */

    function get_sia_progress_status()
    {
        // Load dependencies
        //------------------
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');


        $this->lang->load('clearshare');
        $this->load->library('clearshare/Sia');
        
        try {
            $data = $this->sia->get_siac_consensus();
            $response = $this->sia->get_progress_status();

            if (!empty($response)) {
                $percentage_value = str_replace('Progress (estimated):', '', $response[2]);
                $percentage_value = str_replace('%', '', $percentage_value);
                $height_value = str_replace('Height: ', '', $response[1]);

                $status['results']['Synced'] = $response[0];
                $status['results']['progress_status'] = $response[2];
                $status['results']['height'] = $height_value;
                $status['results']['blocksizelimit'] = $data['currentblock'];
                $status['results']['percentage_value'] = $percentage_value;

            } else {
                $status['results'] = NULL;
            }
        } catch (Exception $e) {
            $status['results'] = NULL;
        }
        
        echo json_encode($status);
    }
}
