<?php

/**
 * OpenSSH server daemon controller.
 *
 * @category   apps
 * @package    ClearSHARE
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Daemon as Daemon_Class;

require clearos_app_base('base') . '/controllers/daemon.php';

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearShare server daemon controller.
 *
 * @category   apps
 * @package    clearshare
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

class Server extends Daemon
{
    /**
     * ClearShare server constructor.
     */

    function __construct()
    {
        parent::__construct('clearshared', 'clearshare');
    }

    /**
     * Default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('base');

        $data['daemon_name'] = 'clearshared';
        $data['app_name'] = 'clearshare';

        // Load views
        //-----------

        $options['javascript'] = array(clearos_app_htdocs('base') . '/daemon.js.php');

        $this->page->view_form('base/daemon', $data, lang('base_server_status'), $options);
    }

    /**
     * Status.
     *
     * @return view
     */

    function status()
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->load->library('clearshare/Clearshared');
        $this->load->library('clearshare/Siad');

        $clearshared_running = $this->clearshared->get_running_state();
        $siad_running = $this->siad->get_running_state();

        $status['status'] = ($clearshared_running && $siad_running) ? Daemon_Class::STATUS_RUNNING : Daemon_Class::STATUS_STOPPED;

        echo json_encode($status);
    }

    /**
     * Start.
     *
     * @return view
     */

    function start()
    {
        $this->load->library('clearshare/Clearshared');
        $this->load->library('clearshare/Siad');

        try {
            // Start siad before clearshared to prevent wallet unlocking issues
            $this->siad->set_running_state(TRUE);
            $this->clearshared->set_running_state(TRUE);
            $this->clearshared->set_boot_state(TRUE);
            $this->siad->set_boot_state(TRUE);
        } catch (Exception $e) {
            // Keep going
        }
    }

    /**
     * Stop.
     *
     * @return view
     */

    function stop()
    {
        $this->load->library('clearshare/Clearshared');
        $this->load->library('clearshare/Siad');

        try {
            // Order is different for normal to facilitate the service watcher as siad can take a long time to shut down
            // and we don't want the service watcher starting it when we are trying to shut it down.
            $this->clearshared->set_boot_state(FALSE);
            $this->siad->set_boot_state(FALSE);
            $this->clearshared->set_running_state(FALSE);
            $this->siad->set_running_state(FALSE);
        } catch (Exception $e) {
            // Keep going
        }
    }
}
