<?php

/**
 * ClearSHARE Host controller.
 *
 * @category   apps
 * @package    ClearSHARE
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearSHARE Host controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage Controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 */

class Clearshare_Host extends ClearOS_Controller
{
    /**
     * ClearSHARE Host controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('clearshare');

        // Load views
        //-----------

        $this->page->view_form("clearshare", NULL, lang('clearshare_app_name'));
    }
}
