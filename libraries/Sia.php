<?php

/**
 * Sia sync info class.
 *
 * @category   apps
 * @package    clearshare
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2008-2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\clearshare;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('clearshare');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Daemon as Daemon;
use \clearos\apps\base\Shell as Shell;
use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\Engine_Exception as Engine_Exception;

clearos_load_library('base/Daemon');
clearos_load_library('base/Shell');
clearos_load_library('base/Engine');
clearos_load_library('base/Engine_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Sia sync info class.
 *
 * @category   apps
 * @package    clearshare
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2008-2021 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

class Sia extends Daemon
{
    /**
     * Sia constructor.
     *
     * @return void
     */

    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////
    

    const COMMAND_CURL = '/usr/bin/curl';
    const COMMAND_SAIC = '/usr/bin/siac';
    const HOST = '127.0.0.1:9980';

    /**
     * construct.
    */
    
    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

    }
    
    /**
     * Returns information about the consensus set.
     *
     * @return @array Array of available host details
    */

    public function get_siac_consensus()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        
        try {

            $retval = $shell->execute(self::COMMAND_CURL, '-A "Sia-Agent" "'.self::HOST.'/consensus"', TRUE, $options);
            $lines = $shell->get_output();

            return $this->_filter_response($lines, 3);

        } catch (Exception $e) {
            throw new Engine_Exception(lang('clearshare_app_aritical_errors'));
        }

    }

    /**
     * Returns information about the consensus set.
     *
     * @return @array Array of available host details
     */

    public function get_progress_status()
    {
        clearos_profile(__METHOD__, __LINE__);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();

        $retval = $shell->execute(self::COMMAND_SAIC, 'consensus', TRUE, $options);
        $lines = $shell->get_output();
        return $lines;
    }

    /**
    * Get filtered response from APIs output
    *
    * @param @json  $response output commands
    * @param @array $index    index of array
    *
    * @return @array response
    */

    protected function _filter_response($response, $index =3)
    {
        $data = json_decode($response[$index], TRUE);
        return $data;
    }
}
